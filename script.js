function copyToClipboard() {
    const copyBox = document.getElementById('copy-box');
    const textArea = document.createElement('textarea');
    textArea.value = copyBox.value;
    document.body.appendChild(textArea);
    textArea.select();
    document.execCommand('copy');
    document.body.removeChild(textArea);
}
